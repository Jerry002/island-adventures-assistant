local assets = {
    Asset("ATLAS", "images/poopstaff.xml"),
	Asset("IMAGE", "images/poopstaff.tex"),
    Asset("ANIM", "anim/poopstaff.zip"),
    Asset("ANIM", "anim/swap_poopstaff.zip"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_poopstaff", "swap_poopstaff")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function onfinished(inst)
    if inst.components.spellcaster then
        inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
    end
    inst:Remove()
end

local function fertiliz(staff, target, pos)
    local caster = staff.components.inventoryitem.owner
    if not caster or not caster.components.inventory or not pos then
        return
    end

    local px, py, pz = pos:Get()
    local prange = 28

    local ents = TheSim:FindEntities(px, py, pz, prange, {"barren"}, {"INLIMBO", "NOCLICK", "fire", "FX", "player"})
    for i, v in ipairs(ents) do
        local fertilizer = caster.components.inventory:FindItem(function(testitem)
            if v:HasTag("witherable_volcanic") then
                return testitem:HasTag("fertilizer_volcanic")
            elseif v:HasTag("witherable_oceanic") then
                return testitem:HasTag("fertilizer_oceanic")
            else
                return testitem:HasTag("fertilizer")
            end
        end)

        if fertilizer and fertilizer.components.fertilizer then
            if v.components.pickable and v.components.pickable:CanBeFertilized() then
                local applied = v.components.pickable:Fertilize(fertilizer, caster)
                TheWorld:PushEvent("CHEVO_fertilized", {target = v, doer = caster})
                if applied then
                    fertilizer.components.fertilizer:OnApplied(caster, v)
                end
            elseif v.components.hackable and v.components.hackable:CanBeFertilized() then
                v.components.hackable:Fertilize(fertilizer, caster)
            end
        end
    end

    if caster.components.sanity then
        caster.components.sanity:DoDelta(-30)
    end

    staff.components.finiteuses:Use(1)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    MakeInventoryFloatable(inst)

    inst.AnimState:SetBank("poopstaff")
    inst.AnimState:SetBuild("poopstaff")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("nopunch")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    MakeHauntableLaunch(inst)

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/poopstaff.xml"
    inst.components.inventoryitem.imagename = "poopstaff"

    inst:AddComponent("tradable")

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetOnFinished(onfinished)
    inst.components.finiteuses:SetMaxUses(10)
    inst.components.finiteuses:SetUses(10)

    inst:AddComponent("spellcaster")
    inst.components.spellcaster.canuseonpoint = true
    inst.components.spellcaster.canuseonpoint_water = true
    inst.components.spellcaster.canuseontargets = false
    inst.components.spellcaster.canusefrominventory = false
    inst.components.spellcaster:SetSpellFn(fertiliz)

    return inst
end

return Prefab("poopstaff", fn, assets)