local WormholePairsNum = {}

local WormholeMarks = Class(function(self, inst)
	if not WormholePairsNum[inst.prefab] then
		WormholePairsNum[inst.prefab] = 0
	end

    self.inst = inst
	self.marked = false
	self.pairs_number = 0
	self.max_pairs_number = 14
	self.mark_icon = inst.prefab
	self.current_pairs_number = WormholePairsNum[inst.prefab]
end)

function WormholeMarks:SetMarkData(icon, max)
	self.mark_icon = icon
	self.max_pairs_number = max or 14
end

function WormholeMarks:IsMarked()
	return self.marked
end

function WormholeMarks:Mark()
	if self.pairs_number <= self.max_pairs_number then
		self.marked = true
		self.inst.MiniMapEntity:SetIcon(self.mark_icon .. self.pairs_number .. ".tex")
	end
end

function WormholeMarks:MarkPairWormhole(other)
	if WormholePairsNum[self.inst.prefab] <= self.max_pairs_number then
		WormholePairsNum[self.inst.prefab] = WormholePairsNum[self.inst.prefab] + 1
		self.pairs_number = WormholePairsNum[self.inst.prefab]
		other.components.wormholemarks.pairs_number = WormholePairsNum[self.inst.prefab]
		self:Mark()
		other.components.wormholemarks:Mark()
	end
end

function WormholeMarks:OnSave()
	local data = {}
	data.marked = self.marked
	data.pairs_number = self.pairs_number
	data.pairs = WormholePairsNum[self.inst.prefab]

	return data
end

function WormholeMarks:OnLoad(data)
	if not data then
		return
	end

	self.marked = data.marked or false
	self.pairs_number = data.pairs_number
	WormholePairsNum[self.inst.prefab] = data.pairs or 0
	if self.marked and self.pairs_number then
		self:Mark()
	end
end

return WormholeMarks