local AddPrefabPostInit = AddPrefabPostInit
GLOBAL.setfenv(1, GLOBAL)

AddPrefabPostInit("knightboat", function(inst)
    if not TheWorld.ismastersim then
        return
    end

    inst.components.health:SetMaxHealth(800)
end)