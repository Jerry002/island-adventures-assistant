
local AddPrefabPostInit = AddPrefabPostInit
GLOBAL.setfenv(1, GLOBAL)

local prefabs = {"flupspawner", "flupspawner_sparse", "flupspawner_dense"}

for _, prefab in ipairs(prefabs) do
    AddPrefabPostInit(prefab, function(inst)
        if not TheWorld.ismastersim then
            return
        end

        inst:Remove()
    end)
end