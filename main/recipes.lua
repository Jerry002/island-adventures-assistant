local AddRecipe2 = AddRecipe2
local MermFisherHouseBuildable = GetModConfigData("MermFisherHouseBuildable")
local TidalPoolBuildable = GetModConfigData("TidalPoolBuildable")
local PoopStaff =  GetModConfigData("PoopStaff")
GLOBAL.setfenv(1, GLOBAL)

local function SortRecipe(a, b, filter_name, offset)
    local filter = CRAFTING_FILTERS[filter_name]
    if filter and filter.recipes then
        for sortvalue, product in ipairs(filter.recipes) do
            if product == a then
                table.remove(filter.recipes, sortvalue)
                break
            end
        end

        local target_position = #filter.recipes + 1
        for sortvalue, product in ipairs(filter.recipes) do
            if product == b then
                target_position = sortvalue + offset
                break
            end
        end

        table.insert(filter.recipes, target_position, a)
    end
end

local function SortAfter(a, b, filter_name)
    SortRecipe(a, b, filter_name, 1)
end

if MermFisherHouseBuildable and softresolvefilepath("scripts/prefabs/mermhouse_fisher.lua") then
    AddRecipe2(
        "mermhouse_fisher_recipe",
        {Ingredient("fish_tropical", 10), Ingredient("rocks", 5), Ingredient("ia_trident", 1), Ingredient("boards", 4)},
        TECH.SCIENCE_TWO,
        {placer = "mermhouse_fisher_placer", product = "mermhouse_fisher", atlas = "images/mermhouse_fisher.xml", image = "mermhouse_fisher.tex"},
        {"STRUCTURES"}
    )
    SortAfter("mermhouse_fisher_recipe", "wildborehouse", "STRUCTURES")
    AddRecipePostInit("mermhouse_fisher_recipe", function(recipe)
        local ingredient = recipe:FindAndConvertIngredient("ia_trident")
        if ingredient then
            ingredient:AddDictionaryPrefab("trident")
        end
    end)
end

if TidalPoolBuildable and softresolvefilepath("scripts/prefabs/tidalpool.lua") then
    AddRecipe2(
        "tidalpool",
        {Ingredient("fish_tropical", 5), Ingredient("cutreeds", 4), Ingredient("turf_tidalmarsh", 4), Ingredient("saltrock", 2)},
        TECH.SCIENCE_TWO,
        {placer = "tidalpool_placer", atlas = "images/tidalpool.xml", image = "tidalpool.tex"},
        {"STRUCTURES"}
    )
    SortAfter("tidalpool", "mermhouse_fisher_recipe", "STRUCTURES")
end

if PoopStaff then
    AddRecipe2(
        "poopstaff",
        {Ingredient("fertilizer", 1), Ingredient("nightmarefuel", 2), Ingredient("livinglog", 3)},
        TECH.MAGIC_TWO,
        {atlas = "images/poopstaff.xml", image = "poopstaff.tex"},
        {"MAGIC"}
    )
    SortAfter("poopstaff", "telestaff", "MAGIC")
end