GLOBAL.setfenv(1, GLOBAL)

CleanList = {  --IA item
	"ia_messagebottleempty",--"空瓶子"
    "vine",--"藤蔓"
    "bamboo",--"竹子"
    "palmleaf",--"棕榈叶"
    "snakeskin",--"蛇皮"
    "venomgland",--"毒腺"
    "jungletreeseed",--"丛林树种"
    "knightboat",--"骑士船"
    "dragoon",--"龙"
    "needlespear",--"仙人掌刺"
    "primeape",--"猿猴"
    "hail_ice",--"冰雹"
}