local AddPrefabPostInit = AddPrefabPostInit
local GetModConfigData = GetModConfigData
local modimport = modimport
GLOBAL.setfenv(1, GLOBAL)

modimport("main/cleanlist")
local cleanperiod = GetModConfigData("CleanPeriod")

local cleanableprefab = {}
for _, prefab in ipairs(CleanList) do
    if GetModConfigData(prefab) then
        cleanableprefab[prefab] = GetModConfigData(prefab)
    end
end

local CleanableEnts = {}
for prefab, _ in pairs(cleanableprefab) do
    AddPrefabPostInit(prefab, function(inst)
        if not TheWorld.ismastersim then
            return
        end

        if not CleanableEnts[prefab] then
            CleanableEnts[prefab] = {}
        end

        CleanableEnts[prefab][inst.GUID] = inst
        inst:ListenForEvent("onremove", function(_inst)
            CleanableEnts[prefab][_inst.GUID] = nil
        end)

        inst:ListenForEvent("onputininventory", function(_inst, owner)
            CleanableEnts[prefab][_inst.GUID] = nil
        end)

        inst:ListenForEvent("ondropped", function(_inst, owner)
            CleanableEnts[prefab][_inst.GUID] = _inst
        end)
    end)
end

local function CleanPrefab()
    TheNet:Announce(STRINGS.CLEANANNOUNCE)  -- 公告

    for prefab, ents in pairs(CleanableEnts) do
        if cleanableprefab[prefab] then
            local cleanable_count = GetTableSize(ents) - cleanableprefab[prefab]
            for _, inst in pairs(ents) do
                if cleanable_count < 0 then
                    break
                end

                if inst:IsValid() and not inst.inlimbo then
                    local x, y, z = inst.Transform:GetWorldPosition()
                    if not FindClosestPlayerInRange(x, y, z, 20) then
                        cleanable_count = cleanable_count - 1
                        inst:Remove()
                    end
                end
            end
        end

    end
end

local function Clean(inst, data)
    if data.name ~= "clean" then
        return
    end

    TheNet:Announce(STRINGS.CLEANNOTICE)

    inst:DoTaskInTime(30, CleanPrefab)
    inst.components.timer:StartTimer("clean", cleanperiod * TUNING.TOTAL_DAY_TIME)  -- 准备下一次清理
end

AddPrefabPostInit("world", function(inst)
    if not inst.ismastersim then
        return
    end

    if not inst.components.timer then
        inst:AddComponent("timer")
    end

    inst.components.timer:StartTimer("clean", cleanperiod * TUNING.TOTAL_DAY_TIME)
    inst:ListenForEvent("timerdone", Clean)
end)