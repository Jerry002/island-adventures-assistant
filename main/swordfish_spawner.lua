local AddPrefabPostInit = AddPrefabPostInit
GLOBAL.setfenv(1, GLOBAL)

AddPrefabPostInit("swordfish_spawner", function(inst)
    inst.entity:AddMiniMapEntity()
    inst.MiniMapEntity:SetIcon("swordfish_spawner.tex")
end)