local AddClassPostConstruct = AddClassPostConstruct
local AddPrefabPostInit = AddPrefabPostInit
local modimport = modimport
GLOBAL.setfenv(1, GLOBAL)

modimport("main/waringevents")
local WaringEvent = require("widgets/waringevent")

local function AddWaringEvents(self)
    for waringevent, data in pairs(WaringEvents) do
        self[waringevent] = self:AddChild(WaringEvent(data.anim))
        self[waringevent]:SetHAnchor(1) -- 设置原点x坐标位置，0、1、2分别对应屏幕中、左、右
        self[waringevent]:SetVAnchor(1) -- 设置原点y坐标位置，0、1、2分别对应屏幕中、上、下
        self[waringevent]:Hide()
    end

    function self:UpdateWaringEvents(eventstime)
        local i = 0
        local line_num = 2
        for waringevent, data in pairs(WaringEvents) do
            local row = math.floor(i/line_num)
            local line = i - row * line_num
            local x =  row * 150
            local y = -line * 70

            self[waringevent]:SetPosition(80 + x, -30 + y, 0)
            local time = eventstime[waringevent]

            if self[waringevent].last_time == time then
                self[waringevent].sametick = (self[waringevent].sametick or 0) + 1
            else
                self[waringevent].sametick = 0
            end


            if (self[waringevent].force == "hide") or
                (self[waringevent].force ~= "show" and (time <= 0 or self[waringevent].sametick >= 100)) then
                if self[waringevent].shown then
                    self[waringevent]:Hide()
                end
            else
                if not self[waringevent].shown then
                    if data.animchangefn then
                        data:animchangefn()
                        self[waringevent]:SetEventAnim(data.anim)
                    end
                    self[waringevent]:Show()
                end
                self[waringevent].last_time = time

                self[waringevent]:OnUpdate(time)

                i = i + 1
            end
        end
    end
end

AddClassPostConstruct("screens/playerhud", AddWaringEvents)

AddPrefabPostInit("forest_network", function(inst)
    if not TheWorld.ismastersim then
        return
    end

	inst:AddComponent("waringtimer")
end)

AddPrefabPostInit("cave_network", function(inst)
    if not TheWorld.ismastersim then
        return
    end

	inst:AddComponent("waringtimer")
end)