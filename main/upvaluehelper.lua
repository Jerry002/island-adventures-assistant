GLOBAL.setfenv(1, GLOBAL)

function GetUpvalueHelper(fn, name)
	local i = 1
	while debug.getupvalue(fn, i) and debug.getupvalue(fn, i) ~= name do
		i = i + 1
	end
	local _name, value = debug.getupvalue(fn, i)
	return value, i
end

function SetUpvalueHelper(start_fn, new_fn, name)
    local start_fn_name, start_fn_i = GetUpvalueHelper(start_fn, name)
    if start_fn_name and start_fn_i then
        -- print(start_fn_name, start_fn_i)
        debug.setupvalue(start_fn, start_fn_i, new_fn)
    end
end
