local AddPrefabPostInit = AddPrefabPostInit
GLOBAL.setfenv(1, GLOBAL)

local function Mark(inst)
    if not TheWorld.ismastersim then
        return
    end

    local teleporter = inst.components.teleporter
    if not teleporter then
        return
    end

    inst:AddComponent("wormholemarks")

    local _RegisterTeleportee = teleporter.RegisterTeleportee
    teleporter.RegisterTeleportee = function(self, ...)  -- ia问题
        _RegisterTeleportee(self, ...)
        local other = teleporter.targetTeleporter

        if not inst.components.wormholemarks:IsMarked() then
            inst.components.wormholemarks:MarkPairWormhole(other)
        end
    end
end

local function tentacle_pillar_postinit(inst)
    if not TheWorld.ismastersim then
        return
    end
    Mark(inst)

    local _SwapToHole = nil
    local OnDeath = nil
    for i, fn in ipairs(inst.event_listening["death"][inst]) do
        _SwapToHole = GetUpvalueHelper(fn, "SwapToHole")
		if _SwapToHole then
            OnDeath = fn
			break
		end
	end

    if not _SwapToHole or not OnDeath then
        return
    end

    local function SwapToHole(_inst, ...)
        local other = _inst.components.teleporter:GetTarget()
        local data = _inst.components.wormholemarks:OnSave()
        _SwapToHole(_inst, ...)

        if data.marked then
            local tentacle_pillar_hole = other.components.teleporter:GetTarget()
            tentacle_pillar_hole.components.wormholemarks:OnLoad(data)
        end
    end

    SetUpvalueHelper(OnDeath, SwapToHole, "SwapToHole")
end

local function tentacle_pillar_hole_postinit(inst)
    if not TheWorld.ismastersim then
        return
    end

    Mark(inst)
    inst.components.wormholemarks:SetMarkData("tentacle_pillar")

    local playerprox = inst.components.playerprox
    if playerprox then
        local TryEmerge = playerprox.onnear
        if TryEmerge then

            local _DoEmerge = GetUpvalueHelper(TryEmerge, "DoEmerge")
            if _DoEmerge then
                local function DoEmerge(_inst, ...)
                    local other = _inst.components.teleporter:GetTarget()
                    local data = _inst.components.wormholemarks:OnSave()
                    _DoEmerge(_inst, ...)

                    local tentacle_pillar = other.components.teleporter:GetTarget()

                    if data.marked then
                        tentacle_pillar.components.wormholemarks:OnLoad(data)
                    end
                end

                SetUpvalueHelper(TryEmerge, DoEmerge, "DoEmerge")
                SetUpvalueHelper(_DoEmerge, DoEmerge, "DoEmerge")
            end
        end
    end
end

AddPrefabPostInit("bermudatriangle", Mark)
AddPrefabPostInit("wormhole", Mark)
AddPrefabPostInit("tentacle_pillar", tentacle_pillar_postinit)
AddPrefabPostInit("tentacle_pillar_hole", tentacle_pillar_hole_postinit)