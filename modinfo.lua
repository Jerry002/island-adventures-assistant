local L = locale
local function en_zh(en, zh)
    return L ~= "zh" and L ~= "zhr" and L ~= "zht" and en or zh
end

name = en_zh("Island Adventures Assistant", "IA辅助")

description = ""
author = "芝麻厨房, Jerry"
version = "0.3"
forumthread = ""

api_version = 10
api_version_dst = 10

priority = 1

--This lets the clients know that they need to download the mod before they can join a server that is using it.
all_clients_require_mod = true

--This let's the game know that this mod doesn't need to be listed in the server's mod listing
client_only_mod = false

--Let the mod system know that this mod is functional with Don't Starve Together
dst_compatible = true

--These tags allow the server running this mod to be found with filters from the server listing screen
server_filter_tags = {"ia_fz","shipwrecked","shipwrecked compatible"}

icon_atlas = "modicon.xml"
icon = "modicon.tex"


configuration_options = {}

--------------------------Boss计时--------------------------
local waringevents = {
    ----------island----------
    "ChessnavySpawn",
    "VolcanoEruption",
    "TwisterAttack",
    "KrakenCooldown",
    "TigersharkCooldown",

    ----------forest----------
    "HoundAttack",
    "DeerclopsAttack",
    "DeerherdSpawn",
    "KlaussackSpawn",
    "AntlionAttack",
    "BeargerSpawn",
    "DragonflySpawn",
    "BeequeenhiveGrown",
    "TerrariumCooldown",
    "MalbatrossSpawn",
    "CrabkingSpawn",

    ----------cave----------
    "ToadstoolRespawn",
    "AtriumgateCooldown",
    "NightmareWild",
}

local waringevents_ch = {
    ----------island----------
    "发条骑士船",
    "火山爆发",
    "豹卷风",
    "海妖",
    "虎鲨",

    ----------forest----------
    "猎犬袭击",
    "巨鹿",
    "无眼鹿群",
    "克劳斯",
    "蚁狮",
    "熊大",
    "龙蝇",
    "蜂后",
    "盒中泰拉",
    "邪天翁",
    "帝王蟹",

    ----------cave----------
    "毒菌蟾蜍",
    "远古大门",
    "暴动",
}
------------------------------------------------------

--------------------------清理--------------------------
local cleanlist = {  --IA item
	"ia_messagebottleempty",--"空瓶子"
    "vine",--"藤蔓"
    "bamboo",--"竹子"
    "palmleaf",--"棕榈叶"
    "snakeskin",--"蛇皮"
    "venomgland",--"毒腺"
    "jungletreeseed",--"丛林树种"
    "knightboat",--"骑士船"
    "dragoon",--"龙"
    "needlespear",--"仙人掌刺"
    "primeape",--"猿猴"
    "hail_ice",--"冰雹"
}

local cleanlist_zh = {  -- 和cleanlist一一对应
    "空瓶子",
    "藤蔓",
    "竹子",
    "棕榈叶",
    "蛇皮",
    "毒腺",
    "丛林树种",
    "骑士船",
    "龙",
    "仙人掌刺",
    "猿猴",
    "冰雹",
}
------------------------------------------------------

local options_enable = {
	{description = en_zh("Disabled", "关闭"), data = false},
	{description = en_zh("Enabled", "开启"), data = true},
}

local count_option = {
	{description = en_zh("Disabled", "关闭"), data = false},
	{description = "5", data = 5},
	{description = "10", data = 10},
	{description = "20", data = 20},
	{description = "30", data = 30},
	{description = "40", data = 40},
	{description = "50", data = 50},
    {description = "70", data = 70},
	{description = "100", data = 100},
}

local function AddOption(name, label_en, label_ch, options, default)
    configuration_options[#configuration_options + 1] = {
        name = name,
		label = en_zh(label_en, label_ch),
		options = options or options_enable,
		default = default == nil and true or default,
	}
end

local function AddBreaker(title)
    AddOption(title, nil, nil, {{description = "", data = false}})
end

AddBreaker(en_zh("Main", "主要功能"))

AddOption("AllIaSurprises", "Add All Ia Surprises", "添加所有ia彩蛋", nil, false)
AddOption("MarksWormhole", "Marks Wormhole and Bermudatriangle and Tentacle_pillar", "标记虫洞, 电光三角, 大触手")
AddOption("BossTimer", "BossTimer", "Boss计时",{
	{description = en_zh("day:m:s", "天:分:秒"), data = 1, hover = en_zh("Game Time", "游戏时间,一天8分钟")},
	{description = en_zh("h:m:s", "时:分:秒"), data = 2, hover = en_zh("Real Time", "现实时间")},
    {description = en_zh("Disabled", "关闭"), data = false},
}, 1)
AddOption("CleanPeriod", "CleanPeriod", "清理周期", count_option, 10)

AddOption("SwordfishSpawnerMapIcon", "Swordfish Mini Map Icon", "剑鱼刷新点小地图图标")
AddOption("KnightBoatHp", "Knight Boat Hp change to 900", "齿轮马血量改为900")

AddOption("RemoveFlupRespawn", "Remove Flup Respawn", "大眼鱼不再生", nil, false)
AddOption("MermFisherHouseBuildable", "Merm Fisher House Buildable", "红鱼人房可制作", nil, false)
AddOption("TidalPoolBuildable", "Tidal Pool Buildable", "潮汐池可制作", nil, false)
AddOption("PoopStaff", "Quick Fertiliz Staff", "快速施肥法杖", nil, false)

AddOption("Language", "Chooese Language","选择中英文", {{description = "中文", data = false}, {description = "English", data = true} }, L ~= "zh" and L ~= "zhr" and L ~= "zht")


AddBreaker(en_zh("TimerOption", "计时器选项"))
for i = 1, #waringevents do
    AddOption(waringevents[i], waringevents[i], waringevents_ch[i])
end

AddBreaker(en_zh("ItemKeepCount", "物品保留数量"))
for i = 1, #cleanlist do
    AddOption(cleanlist[i], cleanlist[i] .. " Keep Count", cleanlist_zh[i] .. "保留数量", count_option, 10)
end