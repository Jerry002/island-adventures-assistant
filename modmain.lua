Assets = {
	Asset("ATLAS", "images/bermudatriangle_marks.xml"),
	Asset("IMAGE", "images/bermudatriangle_marks.tex"),
	Asset("ATLAS", "images/wormhole_marks.xml"),
	Asset("IMAGE", "images/wormhole_marks.tex"),
	Asset("ATLAS", "images/tentacle_pillar_marks.xml"),
	Asset("IMAGE", "images/tentacle_pillar_marks.tex"),
	Asset("ATLAS", "images/mermhouse_fisher.xml"),
	Asset("IMAGE", "images/mermhouse_fisher.tex"),
	Asset("ATLAS", "images/swordfish_spawner.xml"),
	Asset("IMAGE", "images/swordfish_spawner.tex"),
	Asset("ATLAS", "images/tidalpool.xml"),
	Asset("IMAGE", "images/tidalpool.tex"),
}

AddMinimapAtlas("images/bermudatriangle_marks.xml")
AddMinimapAtlas("images/wormhole_marks.xml")
AddMinimapAtlas("images/tentacle_pillar_marks.xml")
AddMinimapAtlas("images/swordfish_spawner.xml")

PrefabFiles = {
	"place",
	"poopstaff"
}

AddReplicableComponent("waringtimer")

modimport("main/upvaluehelper")
modimport("main/recipes")
modimport("main/commands")

if not GetModConfigData("Language") then
	require("languages/chs")
else
	require("languages/en")
end

local IA_CONFIG = GLOBAL.rawget(GLOBAL, "IA_CONFIG")
if IA_CONFIG then
	IA_CONFIG.pondfishable = false
end

if GetModConfigData("MarksWormhole") then
	modimport("main/mark")
end

GLOBAL.TimerMode = GetModConfigData("BossTimer")
if GLOBAL.TimerMode then
	modimport("main/waringevent")
end

if GetModConfigData("CleanPeriod") then
	modimport("main/clean")
end


if GetModConfigData("SwordfishSpawnerMapIcon") then
	modimport("main/swordfish_spawner")
end

if GetModConfigData("RemoveFlupRespawn") then
	modimport("main/flupspawner")
end

if GetModConfigData("KnightBoatHp") then
	modimport("main/knightboat")
end